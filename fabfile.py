from fabric.api import *
import fabric.operations as op
from fabric.contrib.files import exists
from fabric.contrib.files import upload_template
from fabtools import require,files
import time
import os
import datetime
from fabric.utils import fastprint
from StringIO import StringIO
import instances
import itertools
import configserver
import sys



AWS_KEY = 'AKIAIPCYJIDK42KK4TAA'
AWS_SECRET = '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'
import backup
def backup_to_amazon():
    backup.backup_to_amazon_remote()


def restore_from_amazon():
    backup.restore_from_amazon()


def backup_database(password):
    backup.backup_database(password)

def backup_database_nopass():
    backup.backup_database_nopass('')


def copy_landing():
    op.put('landing','/var/www',use_sudo=True)

env.profile = 'base'

def prod_gcp_proxy():
    env.hosts = 'NewGuiche@proxy-gcp.guichevirtual.com.br'
    env.profile += ',prod'
    env.liquibase=True
    env.newrelicName = 'Amazon'
    env.nginx_template = 'templates/nginx-proxy.conf'
    env.skip_backup = False

def configure_server():

    configserver.configure_server()

def reload_nginx():
    fastprint('reloading nginx\n')
    require.nginx.disabled('default')
    require.nginx.enabled('guiche')
    sudo('service nginx reload')

def restart_nginx():
    fastprint('restart_nginx\n')
    require.nginx.disabled('default')
    require.nginx.enabled('guiche')
    sudo('service nginx restart')


def update_nginx_proxy(file_name='templates/nginx-proxy.conf',template_params={}):
    params = template_params.copy()
    #params.update({'servers':env.ip_config})
    upload_template(file_name,'/etc/nginx/sites-available/guiche',params,use_sudo=True,use_jinja=True)
    op.put('itau-ca-root.crt','/etc/nginx/certificates/itau-ca-root.crt',use_sudo=True)
    sudo('service nginx configtest')
    require.nginx.disabled('default')
    require.nginx.enabled('guiche')

def update_dynamic_nginx_proxy(file_name='templates/dynamic-nginx-proxy.conf',template_params={}):
    params = template_params.copy()
    #params.update({'servers':env.ip_config})
    upload_template(file_name,'/etc/nginx/sites-available/guiche',params,use_sudo=True,use_jinja=True)
    #op.put('itau-ca-root.crt','/etc/nginx/certificates/itau-ca-root.crt',use_sudo=True)
    sudo('service nginx configtest')
    require.nginx.disabled('default')
    require.nginx.enabled('guiche')


def clean():
    env.profile = 'base'
