from fabric.api import *
import fabric.operations as op
from fabric.contrib.files import exists
from fabric.contrib.files import upload_template
from fabtools import require,files
import time
import os
import datetime
from fabric.utils import fastprint
from StringIO import StringIO
import itertools
import sys




def prod_santo_anjo():
    env.hosts = 'ubuntu@ec2-18-231-7-13.sa-east-1.compute.amazonaws.com'



def prod_cliente_cantelle():
    env.hosts = 'NewGuiche@rj-gcp-cantelle.guichevirtual.com.br'



def prod_cliente_passaroverde():
    env.hosts = 'NewGuiche@rj-gcp-passaroverde.guichevirtual.com.br'

def prod_cliente_uniaosantacruz():
    env.hosts = 'NewGuiche@rj-gcp-uniaosantacruz.guichevirtual.com.br'

def prod_jbl():
    env.hosts = 'NewGuiche@rj-gcp-jbl.guichevirtual.com.br'

def prod_rapidomarajo():
    env.hosts = 'NewGuiche@rj-gcp-rapidomarajo.guichevirtual.com.br'  

def prod_reunidaspaulista():
    env.hosts = 'NewGuiche@rj-gcp-reunidaspaulista.guichevirtual.com.br'

def prod_mingotti():
    env.hosts = 'NewGuiche@rj-gcp-mingotti.guichevirtual.com.br'

def prod_unidamansur():
    env.hosts = 'NewGuiche@rj-gcp-unidamansur.guichevirtual.com.br'

def prod_viacaoultra():
    env.hosts = 'NewGuiche@rj-gcp-viacaoultra.guichevirtual.com.br'        

def configure_server():
    configserver.configure_server()

@hosts('NewGuiche@rj-gcp-cantelle.guichevirtual.com.br')
def restart_cantelle():
    fastprint('restart_cantelle\n')
    sudo('pkill java || echo "Processo java nao estava rodando"',pty=False)
    time.sleep(10)
    sudo('nohup /dados/jboss/bin/run.sh -c VendaWebRestCantelle -b 0.0.0.0 &> /dev/null &',pty=False)

@hosts('NewGuiche@rj-gcp-cantelle.guichevirtual.com.br')
def reboot_cantelle():
    fastprint('reboot_cantelle\n')
    sudo('sudo reboot',pty=False)
    time.sleep(60)
    sudo('nohup /dados/jboss/bin/run.sh -c VendaWebRestCantelle -b 0.0.0.0 &> /dev/null &',pty=False)

@hosts('NewGuiche@rj-gcp-passaroverde.guichevirtual.com.br')
def restart_passaroverde():
    fastprint('restart_passaroverde\n')
    sudo('pkill java || echo "Processo java nao estava rodando"',pty=False)
    time.sleep(10)
    sudo('nohup /dados/jboss/bin/run.sh -c PassaroVerdeRest -b 0.0.0.0 &> /dev/null &',pty=False)

@hosts('ubuntu@ec2-18-231-7-13.sa-east-1.compute.amazonaws.com')
def restart_santoanjo():
    fastprint('restart_santoanjo\n')
    sudo('/etc/init.d/jboss_rest_guiche_santoanjo stop',pty=False)
    time.sleep(10)
    sudo('/etc/init.d/jboss_rest_guiche_santoanjo start',pty=False)

@hosts('NewGuiche@rj-gcp-uniaosantacruz.guichevirtual.com.br')
def restart_uniaosantacruz():
    fastprint('restart_uniaosantacruz\n')
    sudo('/etc/init.d/jboss_rest_guichevirtual stop' )
    time.sleep(10)
    sudo('/etc/init.d/jboss_rest_guichevirtual start' )

@hosts('NewGuiche@rj-gcp-jbl.guichevirtual.com.br')
def restart_jbl():
    fastprint('restart_jbl\n')
    sudo('/etc/init.d/jboss_rest_jbl stop' )
    time.sleep(10)
    sudo('/etc/init.d/jboss_rest_jbl start' )

@hosts('NewGuiche@rj-gcp-rapidomarajo.guichevirtual.com.br')
def restart_rapidomarajo():
    fastprint('restart_rapidomarajo\n')
    sudo('/etc/init.d/jboss_rest_rapidomarajo stop' )
    time.sleep(10)
    sudo('/etc/init.d/jboss_rest_rapidomarajo start' )

@hosts('NewGuiche@rj-gcp-reunidaspaulista.guichevirtual.com.br')
def restart_reunidaspaulista():
    fastprint('restart_reunidaspaulista\n')
    sudo('/etc/init.d/jboss_rest_reunidas stop' )
    time.sleep(10)
    sudo('/etc/init.d/jboss_rest_reunidas start' ) 

@hosts('NewGuiche@rj-gcp-mingotti.guichevirtual.com.br')
def restart_mingotti():
    fastprint('restart_mingotti\n')
    sudo('/etc/init.d/jboss_rest_mingoti stop' )
    time.sleep(10)
    sudo('/etc/init.d/jboss_rest_mingoti start' )

@hosts('NewGuiche@rj-gcp-unidamansur.guichevirtual.com.br')
def restart_unidamansur():
    fastprint('restart_unidamansur\n')
    sudo('/etc/init.d/jboss_rest_unida_guichevirtual stop' )
    time.sleep(10)
    sudo('/etc/init.d/jboss_rest_unida_guichevirtual start' )

@hosts('NewGuiche@rj-gcp-viacaoultra.guichevirtual.com.br')
def restart_viacaoultra():
    fastprint('restart_viacaoultra\n')
    sudo('/etc/init.d/jboss_rest_ultra stop' )
    time.sleep(10)
    sudo('/etc/init.d/jboss_rest_ultra start' )
