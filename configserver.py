from fabric.api import *
from fabric.contrib import files
import cuisine
from fabtools import require
import fabric.operations as op
import fabtools



def nginx():
	cuisine.package_clean('apache2')
	cuisine.package_ensure('nginx')
	# aguia branca precisa de uma configuracao especial no ssl
	op.put('templates/openssl.cnf','/etc/ssl/openssl.cnf',use_sudo=True)


def certbot():
	cuisine.package_ensure('python3-certbot-nginx')
	

def config_time():
	cuisine.package_ensure('ntp')
	sudo('ln -sf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime')
	sudo('echo "America/Sao_Paulo" | sudo tee /etc/timezone')
	sudo('dpkg-reconfigure --frontend noninteractive tzdata')
	sudo('service ntp restart')


def configure_server(create_mysql=False):
	cuisine.package_update()
	execute(config_time)
	execute(nginx)
	execute(certbot)
	if not files.exists('/opt/gtac'):
		sudo('mkdir /opt/gtac')
