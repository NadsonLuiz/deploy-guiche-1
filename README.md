# Deploy Guiche#

Scripts de deploy do site do guichê virtual. Tornam a tarefa de deploy fácil através do jenkins.

### Ferramentas utilizadas ###
* Python
* Fabric
* Cuisine
* Liquibase
* Maven (dependency plugin)


## Deploy Tasks ##

O deploy de uma nova versão do guichê em um ambiente consiste de várias tarefas. Aqui estão as principais.

* Copia da nova versão: Novo war é copiado para a máquina destino
* Copia de arquivos: arquivos de configuração utilizados pelo site são copiados para o local adequado.
* Atualização Banco de Dados: Liquibase é usado para atualizar a versão do BD
* Reiniciar serviços: alguns serviços precisam ser reiniciados após a atualização para garantir que estão rodando com as configurações indicadas. (nginx, tomcat7)
* Instalação de pacotes: Garantir que tudo que é necessário está intalado na máquina.


## Forma de execução ##
Para utilizar o script de deploy, basta ter o fabric instalado na máquina linux, e executar as task correspondentes.

```
#!python
#envio de nova versão para produção
fab -i chave.pem prod deploy

#atualização de um servidor que funciona de bakcup, apoio
fab - i chave.pem prod slave deploy

```


Algumas tarefas estão separadas em outro arquivo, como o config_server.py. Que cuida que os aplicativos necessários estejam instalados na máquina.


```
#!python
#configuracao de servidor, instalando tudo necessário

fab -f config_server.py -i chave.pem configure_server

```


## Descrição de tarefas ##
### Copia nova versão ###

Através do plugin de dependencias do maven, as últimas versões do site web, admin e liquibase são baixadas para o local de execução.

Após isso, a nova versão é enviada para o servidor de destino.

### Cópia de Arquivos ###

Para a cópia de arquivos de configuração foi feito um esquema de perfis. Alguns dos perfis existentes são:

* prod
* homolog
* slave

Esses perfis configuram tantos  os hosts a serem atualizados como os arquivos a serem utilizados.

Quando se utiliza 2 perfis juntos na hora do deploy, o último perrfil sobresecreve as informações em comum com outro. Isso serve especialmente para o slave, que precisa desabilitar as rotinas que só devem rodas em 1 servidor principal.

Portanto,
```
prod slave deploy # deploy no servidor slave, com configurações de produção
homolog slave deploy # deploy no servidor slave com configurações de homologação
```