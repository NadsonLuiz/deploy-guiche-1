AWS_KEY = 'AKIAJ36QCNANPZ54AISQ'
AWS_SECRET = '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'

from fabric.api import run
from fabric.api import *
import fabric.operations as op
from fabric.contrib.files import exists
import time
import os
import datetime
bucket_name='guichebkp3'
def backup_to_amazon():
    import boto
    with cd('backup'):
        output = run('ls -1 *.gz')
        files = [a for a in output.split() if a.index('.gz')>0]
        print 'files '+str(files)
        if files:
            op.get(files[0],'backup.gz')            
            from boto.s3.key import Key
            conn = boto.connect_s3(AWS_KEY,AWS_SECRET)
            bucket = conn.get_bucket(bucket_name)
            key = Key(bucket)
            today_iso = datetime.date.today().isoformat()
            key.key = 'backup%s.gz'%(today_iso)
            key.set_contents_from_filename('backup.gz')
            run('rm -f dump.sql.gz')

def backup_to_amazon_remote():
    with cd('backup'):
        output = run('ls -1 *.gz')
        files = [a for a in output.split() if a.index('.gz')>0]
        print 'files '+str(files)
        if files:
            name = 'backup%s.gz'%(datetime.date.today().isoformat(),)
            run('s3cmd put %s s3://%s/%s'%(files[0],bucket_name,name))
            run('rm -f dump.sql.gz')
        


def restore_from_amazon():
    import boto
    with cd('backup'):
        conn = boto.connect_s3(AWS_KEY,AWS_SECRET)
        bucket = conn.get_bucket(bucket_name)
        key = bucket.get_key('backup.zip')
        key.get_contents_from_filename('restore.zip')
        run('unzip restore.zip')
        



def backup_database(password):
    with cd('backup'):
        t = datetime.datetime.now()
        run('rm -f dump.sql.gz')
        run('mysqldump --opt --single-transaction --user=root --password=%s --quick guiche | gzip > dump.sql.gz'%(password,))

def backup_database_nopass(password):
    with cd('backup'):
        t = datetime.datetime.now()
        run('rm -f dump.sql.gz')
        run('mysqldump --opt  --single-transaction --quick guiche | gzip > dump.sql.gz')         
